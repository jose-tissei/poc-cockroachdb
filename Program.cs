﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.Extensions.Configuration;
using Npgsql;

namespace cockroachdb_test_app
{
    class Program
    {
        static void Main(string[] args)
        {
            //var connStringBuilder = new NpgsqlConnectionStringBuilder
            //{
            //    Host = "localhost",
            //    Port = 26257,
            //    Username = "root",
            //    Database = "bank",
            //    SslMode = SslMode.Disable
            //};
            ////var connStringBuilder = new NpgsqlConnectionStringBuilder
            ////{
            ////    Host = "localhost",
            ////    Port = 26257,
            ////    Username = "root",
            ////    SslMode = SslMode.Disable
            ////};
            //Simple(connStringBuilder.ConnectionString);

            using (var context = new LivrariaContexto())
            {
                var livros = context.Livros.ToList();
            }
        }

        static void Simple(string connString)
        {
            using (var conn = new NpgsqlConnection(connString))
            {
                conn.Open();

                // Create the "accounts" table.
                new NpgsqlCommand("CREATE TABLE IF NOT EXISTS accounts (id INT PRIMARY KEY, balance INT)", conn).ExecuteNonQuery();

                // Insert two rows into the "accounts" table.
                using (var cmd = new NpgsqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = "UPSERT INTO accounts(id, balance) VALUES(@id1, @val1), (@id2, @val2)";
                    cmd.Parameters.AddWithValue("id1", 1);
                    cmd.Parameters.AddWithValue("val1", 1000);
                    cmd.Parameters.AddWithValue("id2", 2);
                    cmd.Parameters.AddWithValue("val2", 250);
                    cmd.ExecuteNonQuery();
                }

                // Print out the balances.
                System.Console.WriteLine("Initial balances:");
                using (var cmd = new NpgsqlCommand("SELECT id, balance FROM accounts", conn))
                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                        Console.Write("\taccount {0}: {1}\n", reader.GetString(0), reader.GetString(0));
            }
        }
    }

    public class Livro
    {
        public Guid Id { get; set; }
        public string Titulo { get; set; }
        public string Autor { get; set; }
        public long Estoque { get; set; }
    }
    public class LivroMap : IEntityTypeConfiguration<Livro>
    {
        public void Configure(EntityTypeBuilder<Livro> builder)
        {
            builder.Property(m => m.Id);
            builder.Property(c => c.Titulo).HasMaxLength(100).IsRequired();
            builder.Property(c => c.Autor).HasMaxLength(100).IsRequired();
            builder.Property(c => c.Estoque).IsRequired();
        }
    }

    public class LivrariaContexto : DbContext
    {
        public DbSet<Livro> Livros { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new LivroMap());

            base.OnModelCreating(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connStringBuilder = new NpgsqlConnectionStringBuilder
            {
                Host = "localhost",
                Port = 26257,
                Username = "root",
                Database = "bank",
                SslMode = SslMode.Disable
            };

            optionsBuilder.UseNpgsql(connStringBuilder.ConnectionString);
        }
    }
}
